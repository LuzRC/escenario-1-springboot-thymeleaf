package escenario.openshift.escenario1;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Escenario1Application {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Escenario1Application.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
	}

}
