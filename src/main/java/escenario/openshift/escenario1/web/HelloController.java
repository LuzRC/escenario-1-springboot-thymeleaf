package escenario.openshift.escenario1.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	@RequestMapping("/hello")
	public String showHello(Model model) {
		model.addAttribute("saludo", "Aplicación escenario AR01 EI01");
		return "hello";
	}
}
